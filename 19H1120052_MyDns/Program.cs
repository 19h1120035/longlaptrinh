﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
namespace _19H1120052_MyDns
{
    class Program
    {
        static void Main(string[] args)
        {
            string tenHost = Dns.GetHostName();
            Console.WriteLine("Local Host Name: " + tenHost);
            IPHostEntry myself = Dns.GetHostByName(tenHost);
            foreach (IPAddress address in myself.AddressList)
            {
                Console.WriteLine("IP address: {0}", address.ToString());
            }
            Console.ReadKey();
            Console.WriteLine("alo");
        }
    }
}
